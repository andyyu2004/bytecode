#![feature(box_syntax, box_patterns)]
#![feature(const_in_array_repeat_expressions)]
#![feature(vec_remove_item)]
#![allow(unused_unsafe)]

#[macro_use]
extern crate colour;

mod cfg;
mod codegen;
mod debug;
mod error;
mod macros;
mod opt;
mod vm;

use cfg::flags::*;
use codegen::*;
use parserlib::{force_gen_ast_no_typecheck, generate_ast, parse_program, Formatter};
use rustyline::Editor;
use vm::VM;

#[macro_use(lazy_static)]
extern crate lazy_static;

#[macro_use]
macro_rules! exec {
    ($src:expr, $exit:ident) => {{
        let expr = match generate_ast($src) {
            Ok((ty, ast)) => {
                println!("{:?}", ast);
                println!("{}", ast);
                println!("type: {}", ty);
                ast
            }
            Err(errors) => {
                let formatter = Formatter::new($src);
                formatter.write(errors);
                $exit
            }
        };

        let mut bin = Vec::new();
        let mut generator = Generator::new(&mut bin);
        let lambda = generator.gen_expr(expr);

        if *FLAGS & FDEBUG != 0 {
            println!("{:?}", lambda.chunk);
            dark_cyan_ln!("{:#b}", lambda.chunk);
        }

        if *FLAGS & FBYTECODE != 0 {
            $exit
        }

        let mut vm = VM::new(lambda, &bin);
        if let Ok(value) = vm.exec() {
            println!("returning {}", value);
        } else {
            println!("Run had error");
        }
    }};
}

fn main() {
    let mut rl = Editor::<()>::new();

    // println!("sizeof rc ref {}", std::mem::size_of::<std::rc::Rc<std::cell::RefCell<Obj>>>());
    // println!("sizeof vec val {}", std::mem::size_of::<Vec<Value>>());
    // println!("sizeof val {}", std::mem::size_of::<Value>());

    rl.load_history("history.txt").unwrap_or(());

    let args = cfg::parse_args();

    if let Some(file_name) = args.value_of("file") {
        let src = std::fs::read_to_string(file_name).expect("failed to read file");
        if src.is_empty() {
            std::process::exit(0)
        }

        let (types, ast) = parse_program(&src);
        println!("{:?}", ast);
        println!("{:?}", types);

        let mut bin = Vec::new();
        let mut generator = Generator::new(&mut bin);
        let bytecode = generator.generate(ast);

        if *FLAGS & FDEBUG != 0 {
            println!("{:?}", bytecode.chunk);
            dark_cyan_ln!("{:#b}", bytecode.chunk);
        }

        if *FLAGS & FBYTECODE != 0 {
            std::process::exit(0)
        }

        let mut vm = VM::new(bytecode, &bin);
        if let Ok(value) = vm.exec() {
            println!("returning {}", value)
        } else {
            println!("Run had error");
        }
        std::process::exit(0);
    }

    loop {
        // loop reads expressions
        let readline = rl.readline("λ ");
        let line = match readline {
            Ok(line) => {
                rl.add_history_entry(line.as_str());
                rl.save_history("history.txt").unwrap();
                if line.is_empty() {
                    continue;
                }
                line
            }
            Err(_) => break,
        };

        exec!(&line, continue)
    }
}

pub fn force_bytecode(src: &str) -> (Lambda, Vec<Obj>) {
    let ast = force_gen_ast_no_typecheck(src);
    let mut bin = Vec::new();
    let mut generator = Generator::new(&mut bin);
    (generator.gen_expr(ast), bin)
}
