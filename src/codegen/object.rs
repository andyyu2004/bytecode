use super::{Chunk, Value};
use generational_arena::Index;
use std::fmt::{self, Debug, Display, Formatter};
use std::{cell::RefCell, rc::Rc};

#[derive(Clone, PartialEq)]
pub struct Obj {
    pub marked: bool,
    pub kind: ObjKind,
}

impl Obj {
    pub fn new(kind: ObjKind) -> Self {
        Self {
            marked: false,
            kind,
        }
    }
}

impl Display for Obj {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", self.kind)
    }
}
impl Debug for Obj {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{:?}", self.kind)
    }
}

#[derive(Clone, PartialEq)]
pub enum ObjKind {
    Str(String),
    Lambda(Rc<Lambda>),
    Closure(Closure),
    Upvalue(ObjUpvalue),
    Value(Value), // for storing values on the heap
}

impl Display for ObjKind {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            Self::Str(s) => write!(f, "{:?}", s),
            Self::Lambda(l) => write!(f, "{}", l),
            Self::Closure(c) => write!(f, "{}", c),
            Self::Upvalue(p) => write!(f, "{:p}", p),
            Self::Value(value) => write!(f, "{}", value),
        }
    }
}

impl Debug for ObjKind {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            Self::Str(s) => write!(f, "{:?}", s),
            Self::Lambda(l) => write!(f, "{:?}", l),
            Self::Closure(c) => write!(f, "{:?}", c),
            Self::Upvalue(p) => write!(f, "{:p}", p),
            Self::Value(value) => write!(f, "{:?}", value),
        }
    }
}

// Either a standard index(usize) or a generational index
#[derive(Clone, PartialEq, Debug)]
pub enum BinIndex {
    Index(usize),
    GenIndex(Index),
}

impl BinIndex {
    pub fn as_genindex(&self) -> Index {
        if let Self::GenIndex(i) = self {
            *i
        } else {
            panic!()
        }
    }

    pub fn as_index(&self) -> usize {
        if let Self::Index(i) = self {
            *i
        } else {
            panic!()
        }
    }
}

#[derive(Clone, PartialEq, Debug)]
pub struct ObjUpvalue {
    /// this BinIndex either represents a index into the stack or the heap
    /// note that this is different from the BinIndex in the Value Enum
    pub index: Rc<RefCell<BinIndex>>,
}

impl ObjUpvalue {
    pub fn new(index: BinIndex) -> Self {
        Self {
            index: Rc::new(RefCell::new(index)),
        }
    }
}

#[derive(Clone, PartialEq)]
pub struct Lambda {
    pub chunk: Chunk,
    pub upvalue_count: usize,
}

impl Lambda {
    pub fn new() -> Self {
        Self {
            chunk: Chunk::new(),
            upvalue_count: 0,
        }
    }
}

impl Debug for Lambda {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "\nlambda: {{\n{:?}}}", self.chunk)
    }
}

impl Display for Lambda {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "<fn>")
    }
}

#[derive(Clone, PartialEq)]
pub struct Closure {
    pub lambda: Rc<Lambda>,
    pub upvalues: Vec<ObjUpvalue>,
}

impl Closure {
    pub fn new(lambda: Rc<Lambda>) -> Self {
        let upvalues_count = lambda.upvalue_count;
        Self {
            lambda,
            upvalues: Vec::with_capacity(upvalues_count),
        }
    }
}

impl Display for Closure {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "<closure>")
    }
}

impl Debug for Closure {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "closure: {{\n{:?}}}", self.lambda)
    }
}

impl From<String> for Obj {
    fn from(s: String) -> Self {
        Obj::new(ObjKind::Str(s))
    }
}

impl From<Lambda> for Obj {
    fn from(f: Lambda) -> Self {
        Obj::new(ObjKind::Lambda(Rc::new(f)))
    }
}

impl From<Closure> for Obj {
    fn from(f: Closure) -> Self {
        Obj::new(ObjKind::Closure(f))
    }
}

impl From<Value> for Obj {
    fn from(v: Value) -> Self {
        Obj::new(ObjKind::Value(v))
    }
}

impl AsRef<str> for Obj {
    fn as_ref(&self) -> &str {
        if let ObjKind::Str(s) = &self.kind {
            s
        } else {
            panic!()
        }
    }
}
impl AsRef<Rc<Lambda>> for Obj {
    fn as_ref(&self) -> &Rc<Lambda> {
        if let ObjKind::Lambda(f) = &self.kind {
            f
        } else {
            panic!()
        }
    }
}
impl AsRef<Closure> for Obj {
    fn as_ref(&self) -> &Closure {
        if let ObjKind::Closure(f) = &self.kind {
            f
        } else {
            panic!()
        }
    }
}
impl AsRef<ObjUpvalue> for Obj {
    fn as_ref(&self) -> &ObjUpvalue {
        if let ObjKind::Upvalue(u) = &self.kind {
            u
        } else {
            panic!()
        }
    }
}

impl From<f64> for Obj {
    fn from(f: f64) -> Self {
        Value::from(f).into()
    }
}

impl From<Index> for Obj {
    fn from(i: Index) -> Self {
        Value::from(i).into()
    }
}

impl From<i64> for Obj {
    fn from(i: i64) -> Self {
        Value::from(i).into()
    }
}

impl From<bool> for Obj {
    fn from(b: bool) -> Self {
        Value::from(b).into()
    }
}

impl AsRef<Value> for Obj {
    fn as_ref(&self) -> &Value {
        if let ObjKind::Value(v) = &self.kind {
            v
        } else {
            panic!()
        }
    }
}
