use crate::codegen::*;
use parserlib::{BinOp, Binder, Expr, ExprKind, Item, ItemKind, LazyOp, UnaryOp};

pub struct Generator<'a> {
    compilers: Cactus<Compiler>,
    bin: &'a mut Vec<Obj>,
}

impl<'a> Generator<'a> {
    pub(crate) fn new(bin: &'a mut Vec<Obj>) -> Self {
        Generator {
            bin,
            compilers: Cactus::new(Compiler::new()),
        }
    }

    pub(crate) fn gen_expr(&mut self, expr: Expr) -> Lambda {
        self.generate_expr(expr);
        self.compilers.peek_mut().finish()
    }

    pub(crate) fn generate(&mut self, program: Vec<Item>) -> Lambda {
        // we know main exists due to typechecker
        // define all items as variables in "global" scope
        // then add an implicit invocation of main (with no args) to start the program
        program
            .into_iter()
            .for_each(|item| self.generate_item(item));
        self.emit_opcode(Opcode::GGet);
        self.mk_const8("main".to_owned());
        self.emit_opcode(Opcode::Call);
        self.emit_byte(0);
        self.compilers.peek_mut().finish()
    }

    pub(crate) fn generate_item(&mut self, item: Item) {
        match item.kind {
            ItemKind::Fn { params, body, .. } => {
                let (lambda, _) = self.generate_lambda(params, body);
                let index = self.mk_obj(item.name);
                self.emit_const8(lambda);
                self.emit_opcode(Opcode::GSet);
                self.emit_byte(index);
                // pop lambda off the stack, as assignment is an expression
                self.emit_opcode(Opcode::Pop);
            }
            ItemKind::Use { .. } => unimplemented!(),
            ItemKind::Struct { .. } => unimplemented!(),
        }
    }

    fn generate_expr(&mut self, expr: Expr) {
        match expr.kind {
            ExprKind::Unary { op, box expr } => {
                self.generate_expr(expr);
                let opcode = Generator::get_unary_operator_opcode(op);
                self.emit_opcode(opcode);
            }
            ExprKind::Binary {
                op,
                box left,
                box right,
            } => {
                self.generate_expr(left);
                self.generate_expr(right);
                let opcode = Generator::get_binary_operator_opcode(op);
                self.emit_opcode(opcode);
            }
            ExprKind::Let {
                binder, box bound, ..
            } => {
                // Declare local variable by just pushing it on the stack
                self.generate_expr(bound);
                self.compilers.peek_mut().add_local(binder.name);
                self.emit_opcode(Opcode::Unit); // let is an expression in this language
            }
            ExprKind::Lambda {
                params, box body, ..
            } => {
                let (lambda, compiler) = self.generate_lambda(params, body);
                let upvalue_count = lambda.upvalue_count;
                self.emit_opcode(Opcode::Clsr);
                self.mk_const8(lambda);
                for i in 0..upvalue_count {
                    self.emit_byte(if compiler.upvalues[i].in_par { 1 } else { 0 });
                    self.emit_byte(compiler.upvalues[i].index as u8);
                }
            }
            ExprKind::App { box f, args } => {
                assert!(args.len() < 256);
                self.generate_expr(f);
                let argc = args.len();
                args.into_iter().for_each(|expr| self.generate_expr(expr));
                self.emit_opcode(Opcode::Call);
                self.emit_byte(argc as u8);
            }
            ExprKind::Block {
                mut exprs,
                suppressed,
            } => {
                if exprs.is_empty() {
                    return self.emit_opcode(Opcode::Unit);
                }
                self.compilers.peek_mut().enter_scope();
                for expr in exprs.drain(0..exprs.len() - 1) {
                    // these 'expressions' are really expression statements, so pop the value off stack
                    self.generate_expr(expr);
                    self.emit_opt_pop();
                }
                // treat the final expression differently as it is actually an expression and adds a value to the stack
                self.generate_expr(exprs.remove(0));
                // if suppressed ignore the value and return unit instead
                if suppressed {
                    self.emit_opt_pop(); // pop the final expr's value and replace with unit
                    self.exit_scope(false); // no need to save the value; we know it will always be unit
                    self.emit_opcode(Opcode::Unit);
                } else {
                    // at this point in execution the return value of the block is at the top of the stack
                    // we wish to retain this value but pop all the locals below it
                    self.exit_scope(true);
                }
            }
            ExprKind::Id { name } => self.generate_variable(name, false),
            ExprKind::Assn { name, box expr } => {
                self.generate_expr(expr);
                self.generate_variable(name, true);
            }
            ExprKind::If {
                box cond,
                box left,
                right,
            } => {
                self.generate_expr(cond);
                let jmp_index = self.emit_jmp(Opcode::FJMP);
                // then expr
                self.emit_byte(Opcode::Pop); // pop the condition either at the start of then or the start of the (possibly implicit) else
                self.generate_expr(left);
                let else_jmp_index = self.emit_jmp(Opcode::JMP); // skip else branch if then is taken
                                                                 // else expr
                self.patch_jmp(jmp_index);
                self.emit_byte(Opcode::Pop);
                if let Some(box right) = right {
                    self.generate_expr(right);
                }
                self.patch_jmp(else_jmp_index);
            }
            ExprKind::While { box cond, box body } => {
                let loop_start = self.curr_chunk_mut().code.len();
                self.generate_expr(cond);
                let exit_jmp = self.emit_jmp(Opcode::FJMP);
                self.emit_byte(Opcode::Pop);
                self.generate_expr(body);
                // pop expression's value off
                self.emit_opt_pop();
                self.emit_rjmp(loop_start);
                self.patch_jmp(exit_jmp);
                self.emit_byte(Opcode::Pop); // pop cond off on exit also
                                             // while is still an expression, so put a unit on the stack to maintain the invariant
                self.emit_byte(Opcode::Unit);
            }
            ExprKind::Lazy {
                op,
                box left,
                box right,
            } => {
                self.generate_expr(left);
                let jmp = self.emit_jmp(match op {
                    LazyOp::And => Opcode::FJMP,
                    LazyOp::Or => Opcode::TJMP,
                });
                self.emit_byte(Opcode::Pop);
                self.generate_expr(right);
                self.patch_jmp(jmp);
            }
            ExprKind::Tuple { elems } => {
                let n = elems.len();
                for expr in elems {
                    self.generate_expr(expr)
                }
                self.emit_opcode(Opcode::NTup);
                self.emit_byte(n as u8);
            }
            ExprKind::Struct { .. } => unimplemented!(),
            ExprKind::Get { .. } => unimplemented!(),
            ExprKind::Grouping { box expr } => self.generate_expr(expr),
            ExprKind::Integral { value } => self.emit_const8(Value::from(value)),
            ExprKind::Bool { b } => {
                if b {
                    self.emit_opcode(Opcode::True)
                } else {
                    self.emit_opcode(Opcode::False)
                }
            }
            ExprKind::Str { string } => self.emit_const8(string),
        }
    }

    /// generates the code for getting/settings variables
    /// assumes the value to be set is already at the top of the stack
    fn generate_variable(&mut self, name: String, is_set: bool) {
        // search local environment first
        let (get, set, idx) = if let Some(idx) = self.compilers.peek_mut().resolve_var(&name) {
            (Opcode::LGet, Opcode::LSet, idx as u8)
        } else if let Some(idx) = Self::resolve_upvalue(Some(&mut self.compilers), &name) {
            (Opcode::UGet, Opcode::USet, idx as u8)
        } else {
            // else must be global; search by name directly
            (Opcode::GGet, Opcode::GSet, self.mk_obj(name))
        };
        if is_set {
            self.emit_opcode(set)
        } else {
            self.emit_opcode(get)
        }
        self.emit_byte(idx);
    }

    fn generate_lambda(&mut self, params: Vec<Binder>, body: Expr) -> (Lambda, Compiler) {
        self.compilers.push(Compiler::new());
        let compiler = self.compilers.peek_mut();
        compiler.enter_scope();
        params
            .into_iter()
            .for_each(|binder| compiler.add_local(binder.name));
        self.generate_expr(body);
        self.exit_scope(true); // remove all the local variables i.e. parameters off the stack
        let lambda = self.compilers.peek_mut().finish();
        (lambda, self.compilers.pop())
    }

    /// emits pop instruction but may optimize by instead popping a unit instruction directly from the instructions
    /// this is useful in blocks where all but one expression require a pop;
    fn emit_opt_pop(&mut self) {
        if self.curr_chunk().code.last().unwrap() == &Opcode::Unit {
            self.curr_chunk_mut().code.pop();
        } else {
            self.emit_opcode(Opcode::Pop)
        }
    }

    fn exit_scope(&mut self, save: bool) {
        let instructions = self.compilers.peek_mut().exit_scope();
        // if there are no locals to pop; no need to save and unsave
        if instructions.is_empty() {
            return;
        }
        if save {
            self.emit_opcode(Opcode::Save)
        } // save result of the block
        instructions.into_iter().for_each(|op| self.emit_opcode(op));
        if save {
            self.emit_opcode(Opcode::Unsave)
        } // restore the value of the block expression after popping the locals
    }

    fn resolve_upvalue(compiler: Option<&mut Cactus<Compiler>>, name: &str) -> Option<usize> {
        let compiler = compiler?;
        // search in directly enclosing env; note that add_value is passed true indicating it was found in the directly enclosing scope
        // note that the add_upvalue is always called on compiler, never par
        let upvalue_idx = compiler.par.as_mut().and_then(|par| par.lookup(name));
        if let Some(idx) = upvalue_idx {
            compiler.par.as_mut().unwrap().peek_mut().locals[idx].is_captured = true;
            return Some(compiler.peek_mut().add_upvalue(idx, true));
        }
        // otherwise recursively search and add upvalues through compilers, and mark upvalue as not directly enclosing
        let par = compiler.par.as_mut().map(|box par| par);
        Self::resolve_upvalue(par, name).map(|idx| compiler.peek_mut().add_upvalue(idx, false))
    }

    fn curr_chunk_mut(&mut self) -> &mut Chunk {
        &mut self.compilers.peek_mut().lambda.chunk
    }
    fn curr_chunk(&self) -> &Chunk {
        &self.compilers.peek().lambda.chunk
    }

    /// emits the Const8 inst and makes the value
    fn emit_const8(&mut self, obj: impl Into<Obj>) {
        self.emit_opcode(Opcode::Const8);
        self.mk_const8(obj);
    }

    /// add obj to binary; add that index to constant table and return the index into the constant table
    fn mk_obj(&mut self, obj: impl Into<Obj>) -> u8 {
        let index = self.bin.len();
        self.bin.push(obj.into());
        self.curr_chunk_mut().add_const(index.into())
    }

    /// adds the value to the constant pool and emits the index
    fn mk_const8(&mut self, obj: impl Into<Obj>) {
        let obj = obj.into();
        let index = match obj.kind {
            ObjKind::Value(v) => self.curr_chunk_mut().add_const(v),
            _ => self.mk_obj(obj),
        };
        self.emit_byte(index);
    }

    fn emit_byte(&mut self, byte: impl Into<u8>) {
        self.curr_chunk_mut().write_byte(byte.into())
    }

    pub(crate) fn emit_opcode(&mut self, opcode: Opcode) {
        self.emit_byte(opcode)
    }

    /// reverse jmp
    /// base case:
    /// cond_instruction
    /// OP_RJMP <- loop_start
    /// 0x00 arg0 <- code.len()
    /// 0x00 arg1
    /// ip will be here and hence +2 to the offset
    fn emit_rjmp(&mut self, loop_start: usize) {
        self.emit_byte(Opcode::RJMP);
        let offset = self.curr_chunk_mut().code.len() - loop_start + 2;
        if offset as u16 > std::u16::MAX {
            panic!("cannot reverse jump {} (max {})", offset, std::u16::MAX)
        }
        self.emit_byte((offset >> 8) as u8);
        self.emit_byte(offset as u8);
    }

    fn emit_jmp(&mut self, instruction: Opcode) -> usize {
        self.emit_byte(instruction);
        // two placeholder bytes for jmp_offset
        self.emit_byte(0xFF);
        self.emit_byte(0xFF);
        // return the offset of byte after the 3-byte jmp instruction
        self.curr_chunk_mut().code.len()
    }

    /// index is the index of the instruction after the jmp instruction
    fn patch_jmp(&mut self, index: usize) {
        let jmp_offset = self.curr_chunk_mut().code.len() - index;
        if jmp_offset as u16 > std::u16::MAX {
            panic!("cannot jump {} (max {})", jmp_offset, std::u16::MAX)
        }
        self.curr_chunk_mut().code[index - 2] = (jmp_offset >> 8) as u8;
        self.curr_chunk_mut().code[index - 1] = jmp_offset as u8;
    }

    fn get_unary_operator_opcode(op: UnaryOp) -> Opcode {
        match op {
            UnaryOp::Negate => Opcode::Neg,
            UnaryOp::Not => Opcode::Not,
        }
    }

    fn get_binary_operator_opcode(op: BinOp) -> Opcode {
        match op {
            BinOp::Add => Opcode::Add,
            BinOp::Sub => Opcode::Sub,
            BinOp::Mul => Opcode::Mul,
            BinOp::Div => Opcode::Div,
            BinOp::GT => Opcode::GT,
            BinOp::LT => Opcode::LT,
            BinOp::Eq => Opcode::Eq,
            BinOp::NEq => Opcode::Neq,
            BinOp::GTE => Opcode::GTE,
            BinOp::LTE => Opcode::LTE,
            BinOp::BitOr => Opcode::BitOr,
            BinOp::BitAnd => Opcode::BitAnd,
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::force_bytecode;

    #[test]
    fn gen_const() {
        let (lambda, bin) = force_bytecode("5");
        let expected = Chunk {
            code: vec![Opcode::Const8.into(), 0x00, Opcode::Ret.into()],
            constants: vec![Value::Integral(5).into()],
        };
        assert_eq!(lambda.chunk, expected);
    }

    #[test]
    fn gen_unary() {
        let (lambda, bin) = force_bytecode("-5");
        let expected = Chunk {
            code: vec![
                Opcode::Const8.into(),
                0x00,
                Opcode::Neg.into(),
                Opcode::Ret.into(),
            ],
            constants: vec![Value::Integral(5).into()],
        };
        assert_eq!(lambda.chunk, expected);
    }

    #[test]
    fn gen_binary() {
        let (lambda, bin) = force_bytecode("5 + 6");
        let expected = Chunk {
            code: vec![
                Opcode::Const8.into(),
                0x00,
                Opcode::Const8.into(),
                0x01,
                Opcode::Add.into(),
                Opcode::Ret.into(),
            ],
            constants: vec![Value::Integral(5).into(), Value::Integral(6).into()],
        };
        assert_eq!(lambda.chunk, expected);
    }

    #[test]
    fn gen_if_no_else_false_cond() {
        let (lambda, bin) = force_bytecode("if false { 5; }");
        let expected = Chunk {
            code: vec![
                Opcode::False.into(),
                Opcode::FJMP.into(),
                0x00,
                0x08,
                Opcode::Pop.into(),
                Opcode::Const8.into(),
                0x00,
                Opcode::Pop.into(),
                Opcode::Unit.into(),
                Opcode::JMP.into(),
                0x00,
                0x01,
                Opcode::Pop.into(),
                Opcode::Ret.into(),
            ],
            constants: vec![Value::Integral(5).into()],
        };
        assert_eq!(lambda.chunk, expected);
    }

    #[test]
    fn gen_if_with_else_true_cond() {
        let (lambda, bin) = force_bytecode("if true { 5 } else { 4 }");
        let expected = Chunk {
            code: vec![
                Opcode::True.into(),
                Opcode::FJMP.into(),
                0x00,
                0x06,
                Opcode::Pop.into(),
                Opcode::Const8.into(),
                0x00,
                Opcode::JMP.into(),
                0x00,
                0x03,
                Opcode::Pop.into(),
                Opcode::Const8.into(),
                0x01,
                Opcode::Ret.into(),
            ],
            constants: vec![Value::Integral(5).into(), Value::Integral(4).into()],
        };
        assert_eq!(lambda.chunk, expected);
    }

    #[test]
    fn empty_block() {
        let (lambda, bin) = force_bytecode("{}");
        let expected = Chunk {
            code: vec![Opcode::Unit.into(), Opcode::Ret.into()],
            constants: vec![],
        };
        assert_eq!(lambda.chunk, expected);
    }

    #[test]
    fn suppressed_block() {
        let (lambda, _bin) = force_bytecode("{ false; }");
        let expected = Chunk {
            code: vec![
                Opcode::False.into(),
                Opcode::Pop.into(),
                Opcode::Unit.into(),
                Opcode::Ret.into(),
            ],
            constants: vec![],
        };
        assert_eq!(lambda.chunk, expected);
    }
}
