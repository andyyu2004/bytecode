use super::{Lambda, Lookup, Opcode};
use std::fmt::{self, Debug, Formatter};
use std::mem;

pub(crate) struct Compiler {
    pub lambda: Lambda,
    pub locals: Vec<LocalVar>,
    scope_depth: usize,
    pub upvalues: [Upvalue; std::u8::MAX as usize],
}

impl Debug for Compiler {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(
            f,
            "Compiler {{ locals: {:?} upvalues: {:?} }}",
            self.locals,
            &self.upvalues[..self.lambda.upvalue_count]
        )
    }
}

impl Compiler {
    pub(crate) fn new() -> Self {
        Self {
            locals: vec![],
            scope_depth: 0,
            lambda: Lambda::new(),
            upvalues: [Upvalue::default(); 255],
        }
    }

    pub(crate) fn enter_scope(&mut self) {
        self.scope_depth += 1
    }

    pub(crate) fn exit_scope(&mut self) -> Vec<Opcode> {
        let mut opcodes = vec![];
        self.scope_depth -= 1;
        while self.locals.last().map(|x| x.depth) > Some(self.scope_depth) {
            let popped = self.locals.pop().unwrap();
            if popped.is_captured {
                opcodes.push(Opcode::UCls)
            } else {
                opcodes.push(Opcode::Pop)
            }
        }
        opcodes
    }
    pub(crate) fn add_local(&mut self, name: String) {
        self.locals.push(LocalVar::new(name, self.scope_depth))
    }

    pub(crate) fn add_upvalue(&mut self, index: usize, in_par: bool) -> usize {
        // check if upvalue already exists
        if let Some(i) = self
            .upvalues
            .iter()
            .position(|x| x.index == index && x.in_par == in_par)
        {
            return i;
        }
        self.upvalues[self.lambda.upvalue_count] = Upvalue { index, in_par };
        self.lambda.upvalue_count += 1;
        // return index of added upvalue
        self.lambda.upvalue_count - 1
    }

    /// Returns the index of a variable in the locals vector
    /// self.locals mirrors the state of the runtime stack so we can use this index to index the runtime stack to find the variable
    pub(crate) fn resolve_var(&self, tok: &str) -> Option<usize> {
        // Search in reverse as we want the latest declared variable for shadowing purposes
        self.locals.iter().rposition(|v| v.name == tok)
    }

    pub(crate) fn finish(&mut self) -> Lambda {
        self.lambda.chunk.write_byte(Opcode::Ret.into());
        // Optimizer::new(&mut self.lambda).opt();
        mem::replace(&mut self.lambda, Lambda::new())
    }
}

impl Lookup<str> for Compiler {
    type Item = usize;
    fn lookup(&self, key: &str) -> Option<Self::Item> {
        self.resolve_var(key)
    }
}

#[derive(Debug, Clone, PartialEq)]
pub(crate) struct LocalVar {
    name: String,
    depth: usize,
    pub is_captured: bool,
}

impl LocalVar {
    pub fn new(name: String, depth: usize) -> Self {
        Self {
            name,
            depth,
            is_captured: false,
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub(crate) struct Upvalue {
    /// indicates whether this upvalue was found in the directly enclosing env
    pub in_par: bool,
    pub index: usize,
}

impl Default for Upvalue {
    fn default() -> Self {
        Self {
            in_par: false,
            index: std::usize::MAX,
        }
    }
}
