use super::{BinIndex, Closure, Lambda, Obj, ObjKind};
use generational_arena::Index;
use std::fmt::{self, Debug, Display, Formatter};

// the values contained in this enum with the exception of obj act like value types
// i.e. are copied around
#[derive(Clone, PartialEq)]
pub enum Value {
    Unit,
    Double(f64),
    Integral(i64),
    Bool(bool),
    Obj(BinIndex), // usize indicates the object lives in the binary, generation indicates it lives on the heap
}

impl Value {
    pub fn is_obj(&self) -> bool {
        if let Self::Obj(_) = self {
            true
        } else {
            false
        }
    }
}

impl Default for Value {
    fn default() -> Self {
        Self::Unit
    }
}

impl PartialOrd for Value {
    fn partial_cmp(&self, other: &Value) -> Option<std::cmp::Ordering> {
        match (self, other) {
            (Self::Double(x), Self::Double(y)) => x.partial_cmp(y),
            (Self::Integral(x), Self::Integral(y)) => x.partial_cmp(y),
            (Self::Bool(x), Self::Bool(y)) => x.partial_cmp(y),
            _ => None,
        }
    }
}

pub(crate) fn fmt_tuple<T>(elems: &[T], sep: &str) -> String
where
    T: Display,
{
    elems
        .iter()
        .map(|x| x.to_string())
        .collect::<Vec<_>>()
        .join(sep)
}

impl Display for Value {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            Self::Unit => write!(f, "unit"),
            Self::Double(d) => write!(f, "{}", d),
            Self::Integral(i) => write!(f, "{}", i),
            Self::Bool(b) => write!(f, "{}", b),
            Self::Obj(index) => write!(f, "{:?}", index),
        }
    }
}

impl Debug for Value {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            Self::Obj(index) => write!(f, "{:?}", index),
            _ => write!(f, "{}", self),
        }
    }
}

impl From<Value> for f64 {
    fn from(val: Value) -> Self {
        if let Value::Double(float) = val {
            float
        } else {
            panic!()
        }
    }
}

impl From<f64> for Value {
    fn from(f: f64) -> Self {
        Self::Double(f)
    }
}

impl From<usize> for Value {
    fn from(i: usize) -> Self {
        Self::Obj(BinIndex::Index(i))
    }
}

impl From<Index> for Value {
    fn from(i: Index) -> Self {
        Self::Obj(BinIndex::GenIndex(i))
    }
}

impl From<Value> for BinIndex {
    fn from(val: Value) -> Self {
        if let Value::Obj(i) = val {
            i
        } else {
            panic!("expected  found {}", val)
        }
    }
}

impl From<Value> for i64 {
    fn from(val: Value) -> Self {
        if let Value::Integral(i) = val {
            i
        } else {
            panic!("expected i64 found {}", val)
        }
    }
}

impl From<&Value> for i64 {
    fn from(val: &Value) -> Self {
        if let Value::Integral(i) = val {
            *i
        } else {
            panic!()
        }
    }
}

impl From<i64> for Value {
    fn from(i: i64) -> Self {
        Self::Integral(i)
    }
}

impl From<Value> for bool {
    fn from(val: Value) -> Self {
        if let Value::Bool(b) = val {
            b
        } else {
            panic!()
        }
    }
}

impl From<&Value> for bool {
    fn from(val: &Value) -> Self {
        if let Value::Bool(b) = val {
            *b
        } else {
            panic!()
        }
    }
}

impl From<bool> for Value {
    fn from(b: bool) -> Self {
        Self::Bool(b)
    }
}
