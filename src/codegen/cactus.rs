use std::mem;

/// parent-pointer cactus stack
#[derive(Debug, PartialEq)]
pub(crate) struct Cactus<T> {
    pub par: Option<Box<Cactus<T>>>,
    this: T,
}

pub trait Lookup<K: ?Sized> {
    type Item;
    fn lookup(&self, key: &K) -> Option<Self::Item>;
}

impl<T, K: ?Sized, V> Lookup<K> for Cactus<T>
where
    T: Lookup<K, Item = V>,
{
    type Item = V;
    fn lookup(&self, key: &K) -> Option<Self::Item> {
        self.this
            .lookup(key)
            .or_else(|| self.par.as_ref().and_then(|p| p.lookup(key)))
    }
}

impl<T> Cactus<T> {
    pub fn new(this: T) -> Self {
        Cactus { this, par: None }
    }

    pub fn with_parent(this: T, parent: Cactus<T>) -> Self {
        Self {
            par: Some(box parent),
            this,
        }
    }

    pub fn push(&mut self, new: T) {
        let old_self = mem::replace(self, Self::new(new));
        self.par = Some(box old_self);
    }

    pub fn pop(&mut self) -> T {
        let old_par = self.par.take();
        mem::replace(self, *old_par.unwrap()).this
    }

    // pub fn push(self, new: T) -> Self { Self::with_parent(new, self) }
    // pub fn pop(self) -> Self { *self.par.unwrap() }

    pub fn peek(&self) -> &T {
        &self.this
    }
    pub fn peek_mut(&mut self) -> &mut T {
        &mut self.this
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn cactus_push_pop() {
        let mut cactus = Cactus::new(0);
        cactus.push(1);
        cactus.push(2);
        assert_eq!(cactus.this, 2);
        assert_eq!(cactus.par.as_ref().unwrap().this, 1);
        assert_eq!(cactus.par.as_ref().unwrap().par.as_ref().unwrap().this, 0);
        assert_eq!(cactus.pop(), 2);
        assert_eq!(cactus.pop(), 1);
        assert_eq!(cactus.this, 0);
    }
}
