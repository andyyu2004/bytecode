use std::fmt::{self, Formatter, Display};

#[derive(Debug, PartialEq, Copy, Clone, Eq, Hash)]
pub enum Opcode {
    Ret    = 0x00,
    Const8 = 0x01,
    Neg    = 0x02,
    Add    = 0x03,
    Sub    = 0x04,
    Mul    = 0x05,
    Div    = 0x06,
    False  = 0x07,
    True   = 0x08,
    Not    = 0x09,
    Eq     = 0x0a,
    GT     = 0x0b,
    LT     = 0x0c,
    Neq    = 0x0d,
    GTE    = 0x0e,
    LTE    = 0x0f,
    /// LGET <index relative to current stack frame>
    LGet   = 0x10, // get local var
    /// LSET <index relative to current stack frame> (value from stack top)
    LSet   = 0x11,
    /// FJMP <offset> <offset> (16 bit operand)
    FJMP   = 0x12, // jmp on false
    TJMP   = 0x13, // jmp on false
    JMP    = 0x14,
    RJMP   = 0x15,
    Pop    = 0x16,
    Unit   = 0x17,
    /// CALL <argc>
    Call   = 0x18,
    Clsr   = 0x19, // closure
    UGet   = 0x1a, // up-value get & set
    USet   = 0x1b,
    BitOr  = 0x1c,
    BitAnd = 0x1d,
    UCls   = 0x1e, // close over upvalue
    Save   = 0x1f,
    Unsave = 0x20, // pair of instructions to temporarily pop a value off the stack into another stack (save) and then restore it back to the main stack (unsave)
    /// GGET <name_const_index>
    GGet   = 0x21,
    /// GSET <name_const_index> (value from top of stack)
    GSet   = 0x22,
    /// NTUP <elem_count>
    NTup   = 0x23,

}

impl Display for Opcode {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl PartialEq<u8> for Opcode {
    fn eq(&self, rhs: &u8) -> bool { &u8::from(self) == rhs }
}

impl PartialEq<Opcode> for u8 {
    fn eq(&self, rhs: &Opcode) -> bool { self == &u8::from(rhs) }
}

impl From<u8> for Opcode {
    fn from(x: u8) -> Self {
        match x {
            0x00  => Self::Ret,
            0x01  => Self::Const8,
            0x02  => Self::Neg,
            0x03  => Self::Add,
            0x04  => Self::Sub,
            0x05  => Self::Mul,
            0x06  => Self::Div,
            0x07  => Self::False,
            0x08  => Self::True,
            0x09  => Self::Not,
            0x0a => Self::Eq,
            0x0b => Self::GT,
            0x0c => Self::LT,
            0x0d => Self::Neq,
            0x0e => Self::GTE,
            0x0f => Self::LTE,
            0x10 => Self::LGet,
            0x11 => Self::LSet,
            0x12 => Self::FJMP,
            0x13 => Self::TJMP,
            0x14 => Self::JMP,
            0x15 => Self::RJMP,
            0x16 => Self::Pop,
            0x17 => Self::Unit,
            0x18 => Self::Call,
            0x19 => Self::Clsr,
            0x1a => Self::UGet,
            0x1b => Self::USet,
            0x1c => Self::BitOr,
            0x1d => Self::BitAnd,
            0x1e => Self::UCls,
            0x1f => Self::Save,
            0x20 => Self::Unsave,
            0x21 => Self::GGet,
            0x22 => Self::GSet,
            0x23 => Self::NTup,
            x => panic!("Invalid opcode `{:#x}`", x),
        }
    }
}

impl From<Opcode> for u8 { fn from(opcode: Opcode) -> Self { opcode as u8 } }
impl From<&Opcode> for u8 { fn from(opcode: &Opcode) -> Self { *opcode as u8 } }

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_conversions() {
        let max_op_code = 0x22;
        for i in 0..=max_op_code {
            assert_eq!(i, u8::from(Opcode::from(i)));
        }
    }

    #[test]
    fn test_cast() {
        assert_eq!(Opcode::Div as u8, 6u8)
    }

}


