use super::Value;
use crate::debug::disassemble_chunk;
use std::fmt::{self, Binary, Debug, Formatter};

#[derive(PartialEq, Clone, Default)]
pub struct Chunk {
    pub code: Vec<u8>,
    pub constants: Vec<Value>,
}

impl Chunk {
    pub fn new() -> Self {
        Chunk {
            code: vec![],
            constants: vec![],
        }
    }

    /// Appends a byte to the chunk's code
    pub fn write_byte(&mut self, byte: u8) {
        self.code.push(byte)
    }

    /// Appends a obj to the constant pool
    /// Returns index of constant in the vector
    pub fn add_const(&mut self, val: Value) -> u8 {
        self.constants.push(val);
        if self.constants.len() > 256 {
            panic!("Too many constants in one chunk")
        }
        (self.constants.len() - 1) as u8
    }
}

impl Binary for Chunk {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            self.code
                .iter()
                .map(|x| format!("{:08b}", x))
                .collect::<Vec<_>>()
                .join(" ")
        )
    }
}

impl Debug for Chunk {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        disassemble_chunk(self, f)?;
        writeln!(f, "\tConstants {:?}", self.constants)
    }
}
