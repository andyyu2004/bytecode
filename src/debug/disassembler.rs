use crate::codegen::{Chunk, Lambda, Obj, Opcode};
use std::cell::{Ref, RefCell};
use std::fmt::{self, Formatter};
use std::rc::Rc;

pub fn disassemble_chunk(chunk: &Chunk, f: &mut Formatter) -> fmt::Result {
    let mut offset = 0;
    while offset < chunk.code.len() {
        offset = disassemble_instruction(chunk, offset, f)?
    }
    Ok(())
}

/// returns the offset of the next instruction
pub fn disassemble_instruction(
    chunk: &Chunk,
    offset: usize,
    f: &mut Formatter,
) -> Result<usize, fmt::Error> {
    let instruction = chunk.code[offset];
    match instruction.into() {
        Opcode::Ret
        | Opcode::Add
        | Opcode::Sub
        | Opcode::Mul
        | Opcode::Div
        | Opcode::Not
        | Opcode::Neg
        | Opcode::False
        | Opcode::True
        | Opcode::Neq
        | Opcode::Eq
        | Opcode::GTE
        | Opcode::GT
        | Opcode::LT
        | Opcode::Pop
        | Opcode::Unit
        | Opcode::BitAnd
        | Opcode::BitOr
        | Opcode::UCls
        | Opcode::Save
        | Opcode::Unsave
        | Opcode::NTup
        | Opcode::LTE => single_byte_instruction(instruction, offset, f),
        Opcode::JMP | Opcode::RJMP | Opcode::FJMP | Opcode::TJMP => {
            jmp_instruction(chunk, offset, f)
        }
        Opcode::LGet
        | Opcode::UGet
        | Opcode::GSet
        | Opcode::GGet
        | Opcode::USet
        | Opcode::LSet
        | Opcode::Call => double_byte_instruction(chunk, offset, f),
        Opcode::Const8 => dis_const_8(chunk, offset, f),
        Opcode::Clsr => closure_instruction(chunk, offset, f),
    }
}

fn read16(xs: &[u8]) -> u16 {
    let fst = xs[0] as u16;
    let snd = xs[1] as u16;
    (fst << 8) | snd
}

fn single_byte_instruction(
    instruction: u8,
    offset: usize,
    f: &mut Formatter,
) -> Result<usize, fmt::Error> {
    writeln!(
        f,
        "\t{:#x}\t{:#04x} {}",
        offset,
        instruction,
        Opcode::from(instruction)
    )?;
    Ok(offset + 1)
}

fn read_byte(offset: &mut usize, chunk: &Chunk) -> u8 {
    let byte = chunk.code[*offset];
    *offset += 1;
    byte
}

fn closure_instruction(
    chunk: &Chunk,
    mut offset: usize,
    f: &mut Formatter,
) -> Result<usize, fmt::Error> {
    let initial_offset = offset;
    let instruction = read_byte(&mut offset, chunk);
    let next = read_byte(&mut offset, chunk) as usize;

    writeln!(
        f,
        "\t{:#x}\t{:#04x} {} {:#04x}",
        initial_offset,
        instruction,
        Opcode::from(instruction),
        next
    )?;

    // let lambda: &Rc<Lambda> = &chunk.constants[next].as_ref();
    // for _ in 0..lambda.upvalue_count {
    //     let in_par = read_byte(&mut offset, chunk);
    //     let desc = if in_par == 1 { "local" } else { "upvalue" };
    //     let index = read_byte(&mut offset, chunk);
    //     writeln!(f, "\t|\t{} {}", desc, index)?;
    // }
    Ok(offset)
}

fn jmp_instruction(chunk: &Chunk, offset: usize, f: &mut Formatter) -> Result<usize, fmt::Error> {
    let instruction = chunk.code[offset];
    let jmp_offset = read16(&chunk.code[offset + 1..]) as usize;
    let new_ip = if instruction == Opcode::RJMP {
        offset + 3 - jmp_offset
    } else {
        offset + 3 + jmp_offset
    };
    writeln!(
        f,
        "\t{:#x}\t{:#04x} {} {:#x} ({:#x} -> {:#x})",
        offset,
        instruction,
        Opcode::from(instruction),
        jmp_offset,
        offset + 3,
        new_ip
    )?;
    Ok(offset + 3)
}

fn double_byte_instruction(
    chunk: &Chunk,
    offset: usize,
    f: &mut Formatter,
) -> Result<usize, fmt::Error> {
    let instruction = chunk.code[offset];
    let next = chunk.code[offset + 1] as usize;
    // Debug trait prepends 0x, want width 4 as a byte will be repr in 2 hex digits
    writeln!(
        f,
        "\t{:#x}\t{:#04x} {} {:#04x}",
        offset,
        instruction,
        Opcode::from(instruction),
        next
    )?;
    Ok(offset + 2)
}

fn dis_const_8(chunk: &Chunk, offset: usize, f: &mut Formatter) -> Result<usize, fmt::Error> {
    let instruction = chunk.code[offset];
    let next = chunk.code[offset + 1] as usize;
    writeln!(
        f,
        "\t{:#x}\t{:#04x} {} {:#04x} (val = {})",
        offset,
        instruction,
        Opcode::from(instruction),
        next,
        chunk.constants[next]
    )?;
    Ok(offset + 2)
}
