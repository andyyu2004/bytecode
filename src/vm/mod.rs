pub(crate) mod stack;

use crate::cfg::flags::*;
use crate::codegen::*;
use crate::error::InterpretError;
use generational_arena::{Arena, Index};
use stack::StackFrame;
use std::collections::HashMap;
use std::mem;
use std::ops;
use std::rc::Rc;

const STACK_SIZE: usize = 100;
// const STACK_FRAMES : usize = 64;

pub struct VM<'a> {
    stack: [Value; STACK_SIZE],
    stack_ptr: usize, // points to top of stack (one past the top element)
    // frames: [StackFrame; STACK_FRAMES],
    frames: Vec<StackFrame>,
    save_stack: Vec<Value>,
    open_upvalues: Vec<ObjUpvalue>,
    globals: HashMap<String, Value>,
    // for debug
    op_code_counts: HashMap<Opcode, u64>,
    heap: Arena<Obj>,
    bin: &'a Vec<Obj>,
}

impl<'a> VM<'a> {
    pub fn new(lambda: Lambda, bin: &'a Vec<Obj>) -> Self {
        let lambda = Rc::new(lambda);
        VM {
            bin,
            frames: vec![StackFrame::new(Closure::new(lambda), 0)],
            stack: [Value::Unit; STACK_SIZE],
            open_upvalues: vec![],
            save_stack: vec![],
            stack_ptr: 0,
            globals: HashMap::new(),
            op_code_counts: HashMap::new(),
            heap: Arena::new(),
        }
    }

    pub fn exec(&mut self) -> Result<Obj, InterpretError> {
        let mut frame: *mut StackFrame = self.curr_frame_mut();

        macro_rules! read_byte {
            () => {{
                let byte = peek_byte!();
                jmp!(1);
                byte
            }};
        }

        macro_rules! frame {
            () => {
                unsafe { &*frame }
            };
        }

        macro_rules! frame_mut {
            () => {
                unsafe { &mut *frame }
            };
        }

        macro_rules! peek_byte {
            () => {
                unsafe { *frame!().ip }
            };
        }
        macro_rules! read_const8 {
            () => {
                read_const8_ref!().clone()
            };
        }
        macro_rules! get_relative {
            ($idx:expr) => {
                unsafe { self.stack[$idx + frame!().offset].clone() }
            };
        }
        macro_rules! get_relative_mut {
            ($idx:expr) => {
                unsafe { &mut self.stack[$idx + frame!().offset] }
            };
        }

        /// read a constant for the current chunks constant pool
        /// the offset of the value in the pool is contained in the byte after the Const8 opcode
        macro_rules! read_const8_ref {
            () => {{
                let index = read_byte!() as usize;
                &frame!().closure.lambda.chunk.constants[index]
            }};
        }

        macro_rules! read16 {
            () => {{
                let fst = read_byte!() as u16;
                let snd = read_byte!() as u16;
                (fst << 8) | snd
            }};
        }

        macro_rules! jmp {
            ($offset:expr) => {
                unsafe { frame_mut!().ip = frame!().ip.offset($offset as isize) }
            };
        }

        let ret = loop {
            if *FLAGS & FDEBUG != 0 {
                let opcode = peek_byte!().into();
                *self.op_code_counts.entry(opcode).or_default() += 1;
                if *FLAGS & FVERBOSE != 0 {
                    self.print_stack()
                }
            }

            match read_byte!().into() {
                Opcode::Const8 => self.push(read_const8!()),
                Opcode::Not => {
                    let b = !bool::from(self.pop());
                    self.push(b.into());
                }
                Opcode::LGet => {
                    let idx = read_byte!() as usize;
                    self.push(get_relative!(idx));
                }
                Opcode::FJMP => {
                    let offset = read16!() as i16;
                    if !bool::from(self.peek_val()) {
                        jmp!(offset)
                    }
                }
                Opcode::TJMP => {
                    let offset = read16!() as i16;
                    if bool::from(self.peek_val()) {
                        jmp!(offset)
                    }
                }
                Opcode::Clsr => {
                    let bin_index: BinIndex = read_const8!().into();
                    let lambda: &Rc<Lambda> = self.deref_binindex(bin_index).as_ref();
                    let mut closure = Closure::new(Rc::clone(lambda));

                    for _i in 0..closure.lambda.upvalue_count {
                        let in_par = read_byte!() == 1;
                        let index = read_byte!() as usize;
                        if in_par {
                            let offset = frame!().offset + index;
                            let upvalue = self.capture_upvalue(offset);
                            // pushing is same as setting index as the closure starts with an empty array
                            // closure.upvalues[i] = upvalue;
                            closure.upvalues.push(upvalue);
                        } else {
                            // closure.upvalues[i] = closure.upvalues[index];
                            closure.upvalues.push(closure.upvalues[index].clone());
                        }
                    }

                    let index = self.heap.insert(closure.into());
                    self.push(Value::Obj(BinIndex::GenIndex(index)))
                }
                Opcode::Call => {
                    let argc = read_byte!() as usize;
                    // we can find the function as we know it is directly before the arguments
                    let index: BinIndex = self.stack[self.stack_ptr - argc - 1].clone().into();
                    let obj = self.deref_binindex(index);
                    let closure = match &obj.kind {
                        // for calling top-level functions which are not closures; access only locals and globals and can be hoisted
                        // but wrap in empty closure to match types
                        ObjKind::Lambda(lambda) => Closure::new(Rc::clone(lambda)),
                        ObjKind::Closure(f) => f.clone(), // clone the closure as every instance will close over different values
                        _ => unreachable!(),
                    };

                    // let the stack frame offset be one past the lambda so the arguments are in the correct relative index for local variables
                    let stack_frame = StackFrame::new(closure, self.stack_ptr - argc);
                    self.frames.push(stack_frame);
                    frame = self.frames.last_mut().unwrap();
                }
                Opcode::Ret => {
                    let ret = self.pop();
                    // close over all open upvalues that are in the frame that is about to get popped
                    self.close_upvalues(frame!().offset);
                    self.frames.pop();
                    // if finished executing top level function
                    if self.frames.is_empty() {
                        break ret;
                    }
                    frame = self.curr_frame_mut();
                    self.pop(); // pop the function also; can alternatively manually set the frame pointer to restore to where the frame offset is
                    self.push(ret);
                }
                Opcode::Save => {
                    let v = self.pop();
                    self.save_stack.push(v);
                }
                Opcode::Unsave => {
                    let v = self.save_stack.pop().unwrap();
                    self.push(v);
                }
                Opcode::UCls => {
                    self.close_upvalues(self.stack_ptr - 1);
                    self.pop();
                }
                Opcode::GGet => {
                    let bin_index: BinIndex = read_const8!().into();
                    let obj = self.deref_binindex(bin_index);
                    let s: &str = obj.as_ref();
                    let v = self.globals.get(s).unwrap().clone();
                    self.push(v);
                }
                Opcode::GSet => {
                    let bin_index: BinIndex = read_const8!().into();
                    let obj = self.deref_binindex(bin_index);
                    let s: &str = obj.as_ref();
                    let s = s.to_owned();
                    let v = self.peek_val().clone();
                    self.globals.insert(s, v);
                }
                Opcode::NTup => {
                    let n = read_byte!() as usize;
                    let mut v = vec![Value::Integral(0); n];
                    for i in (0..n).rev() {
                        v[i] = self.pop()
                    }
                    unimplemented!();
                    // self.push(Value::Tuple(v))
                }
                Opcode::LSet => {
                    let index = read_byte!() as usize;
                    *get_relative_mut!(index) = self.peek_val().clone();
                }
                Opcode::UGet => {
                    let index = read_byte!() as usize;
                    let gen_index = frame!().closure.upvalues[index].index.borrow();

                    match &*gen_index {
                        BinIndex::Index(i) => self.push(self.stack[*i].clone()),
                        BinIndex::GenIndex(i) => {
                            // we know the upvalue index points to a Value on the ObjHeap
                            // we want to push the value itself on the stack not the index
                            let val: &Value = self.heap[*i].as_ref();
                            let val = val.clone();
                            self.push(val)
                        }
                    };
                }
                Opcode::USet => {
                    let index = read_byte!() as usize;
                    let value = self.stack[self.stack_ptr - 1].clone();
                    let gen_index = &frame_mut!().closure.upvalues[index].index;
                    match &*gen_index.borrow() {
                        BinIndex::Index(i) => self.stack[*i] = value,
                        BinIndex::GenIndex(i) => self.heap[*i] = value.into(),
                    }
                }
                Opcode::JMP => {
                    // care: offset reading must not be one lined as the ip is changing while reading
                    let offset = read16!() as isize;
                    jmp!(offset)
                }
                Opcode::RJMP => {
                    let offset = read16!() as isize;
                    jmp!(-offset);
                }
                Opcode::Pop => self.stack_ptr -= 1,
                Opcode::Add => self.integral_arithmetic(ops::Add::add),
                Opcode::Sub => self.integral_arithmetic(ops::Sub::sub),
                Opcode::Mul => self.integral_arithmetic(ops::Mul::mul),
                Opcode::Div => self.integral_arithmetic(ops::Div::div),
                Opcode::BitAnd => self.integral_arithmetic(ops::BitAnd::bitand),
                Opcode::BitOr => self.integral_arithmetic(ops::BitOr::bitor),
                Opcode::Neg => self.integral_unary(ops::Neg::neg),
                Opcode::Eq => self.eq_operator(|x, y| x == y),
                Opcode::Neq => self.eq_operator(|x, y| x != y),
                Opcode::LTE => self.cmp_operator(|x, y| x <= y),
                Opcode::LT => self.cmp_operator(|x, y| x < y),
                Opcode::GTE => self.cmp_operator(|x, y| x >= y),
                Opcode::GT => self.cmp_operator(|x, y| x > y),
                Opcode::False => self.push(Value::Bool(false)),
                Opcode::True => self.push(Value::Bool(true)),
                Opcode::Unit => self.push(Value::Unit),
            };
        };

        if *FLAGS & FDEBUG != 0 {
            let mut vec = self
                .op_code_counts
                .iter()
                .map(|(op, i)| (*op, *i))
                .collect::<Vec<(Opcode, u64)>>();
            vec.sort_by(|(_, i), (_, j)| j.cmp(i));
            println!(
                "{:#?}",
                vec.iter()
                    .map(|(op, count)| format!("{} => {}", op, count))
                    .collect::<Vec<String>>()
            );
            println!(
                "instructions executed: {}",
                self.op_code_counts.values().sum::<u64>()
            );
        }

        let obj = match ret {
            Value::Obj(index) => self.deref_binindex(index).clone(),
            x => x.into(),
        };

        Ok(obj)
    }

    fn deref_binindex(&self, index: BinIndex) -> &Obj {
        match index {
            BinIndex::Index(index) => &self.bin[index],
            BinIndex::GenIndex(index) => &self.heap[index],
        }
    }

    fn push(&mut self, value: Value) {
        self.stack[self.stack_ptr] = value;
        self.stack_ptr += 1;
    }

    fn pop(&mut self) -> Value {
        self.stack_ptr -= 1;
        mem::take(&mut self.stack[self.stack_ptr])
    }

    fn peek_val(&mut self) -> &Value {
        &self.stack[self.stack_ptr - 1]
    }

    fn curr_frame(&self) -> &StackFrame {
        self.frames.last().unwrap()
    }
    fn curr_frame_mut(&mut self) -> &mut StackFrame {
        self.frames.last_mut().unwrap()
    }

    /// takes a pointer into the stack and closes over every upvalue above the ptr
    fn close_upvalues(&mut self, offset: usize) {
        // this impl only closes over the one being pointed to
        if let Some(pos) = self
            .open_upvalues
            .iter()
            .rposition(|x| x.index.borrow().as_index() == offset)
        {
            let upvalue = self.open_upvalues.remove(pos);
            let value = mem::replace(
                &mut self.stack[upvalue.index.borrow().as_index()],
                Value::default(),
            );
            let gen_index = self.heap.insert(value.into());
            *upvalue.index.borrow_mut() = BinIndex::GenIndex(gen_index);
        }
        // type V = Vec<Rc<RefCell<Upvalue>>>;
        // let open_upvalues = mem::replace(&mut self.open_upvalues, vec![]);
        // let (open, to_close): (V, V) = open_upvalues.into_iter().partition(|v| v.borrow().ptr < stack_ptr);
        // mem::replace(&mut self.open_upvalues, open);
        // for upvalue in to_close {
        //     // add the value to be closed to the upvalue and update the upvalue pointer to point to its own field (this is safe regarding the raw pointer)
        //     let value_ptr = upvalue.borrow().ptr;
        //     let value = unsafe { &*value_ptr }.clone();
        //     upvalue.borrow_mut().closed = Some(box value);
        //     let location: *mut Value = upvalue.borrow_mut().closed.as_mut().unwrap().as_mut();
        //     upvalue.borrow_mut().ptr = location;
        // }
    }

    /// capture_upvalue at `offset` in stack
    fn capture_upvalue(&mut self, offset: usize) -> ObjUpvalue {
        let upvalue = ObjUpvalue::new(BinIndex::Index(offset));
        if let Some(t) = self
            .open_upvalues
            .iter_mut()
            .rfind(|x| x.index.borrow().as_index() == offset)
        {
            return t.clone();
        }
        self.open_upvalues.push(upvalue.clone());
        upvalue
    }

    fn integral_unary(&mut self, f: impl Fn(i64) -> i64) {
        let x: i64 = self.pop().into();
        let value = Value::Integral(f(x));
        self.push(value);
    }

    fn eq_operator(&mut self, f: impl Fn(Value, Value) -> bool) {
        let r = self.pop();
        let l = self.pop();
        self.push(Value::Bool(f(l, r)))
    }

    fn cmp_operator(&mut self, f: impl Fn(Value, Value) -> bool) {
        let r = self.pop();
        let l = self.pop();
        self.push(Value::Bool(f(l, r)))
    }

    fn integral_arithmetic(&mut self, f: impl Fn(i64, i64) -> i64) {
        let r: i64 = self.pop().into();
        let l: i64 = self.pop().into();
        let value = Value::Integral(f(l, r));
        self.push(value);
    }

    /// prints the state of the stack before executing the instruction indicated by the ip
    fn print_stack(&self) {
        println!(
            "\t{}",
            self.stack[..self.stack_ptr]
                .iter()
                .enumerate()
                .rev()
                .map(|(i, x)| format!(
                    "{} {}",
                    x,
                    if i == self.curr_frame().offset {
                        "<-"
                    } else {
                        ""
                    }
                ))
                .collect::<Vec<String>>()
                .join("\n\t")
        );
        println!(
            "\tip: {:#x} {}\n",
            self.ip_offset(),
            Opcode::from(unsafe { *self.curr_frame().ip })
        );
    }

    /// returns offset of the ip from the start of the chunk
    fn ip_offset(&self) -> usize {
        let ptr: *const u8 = &self.curr_frame().closure.lambda.chunk.code[0];
        let x = self.curr_frame().ip as usize;
        x - ptr as usize
    }
}

#[cfg(test)]
mod test {
    use super::*;

    fn exec(src: &str) -> Obj {
        let (bytecode, bin) = crate::force_bytecode(src);
        let mut vm = VM::new(bytecode, &bin);
        vm.exec().unwrap()
    }

    fn exec_prog(src: &str) -> Obj {
        let ast = crate::parse_program(src).1;
        let mut bin = Vec::new();
        let bytecode = crate::Generator::new(&mut bin).generate(ast);
        VM::new(bytecode, &bin).exec().unwrap()
    }

    #[test]
    fn test_logical_or() {
        assert_eq!(exec("false || false"), false.into());
        assert_eq!(exec("false || true"), true.into());
        assert_eq!(exec("true  || false"), true.into());
        assert_eq!(exec("true  || true"), true.into());
    }

    #[test]
    fn test_logical_and() {
        assert_eq!(exec("false && false"), false.into());
        assert_eq!(exec("false && true"), false.into());
        assert_eq!(exec("true  && false"), false.into());
        assert_eq!(exec("true  && true"), true.into());
    }

    #[test]
    fn test_arith() {
        assert_eq!(exec("2 + 3"), 5.into());
        assert_eq!(exec("2 - 3"), (-1).into());
        assert_eq!(exec("6 / 2"), 3.into());
        assert_eq!(exec("5 * 3"), 15.into());
    }

    #[test]
    fn simple_calls() {
        assert_eq!(exec("(fn x => x)(4)"), 4.into());
        assert_eq!(exec("(fn (x, y) => x)(1, 2)"), 1.into());
        assert_eq!(exec("(fn (x, y) => y)(1, 2)"), 2.into());
    }

    #[test]
    fn nested_calls() {
        assert_eq!(exec("(fn x => fn y => y)(4)(5)"), 5.into());
        assert_eq!(exec("{ let inc = fn i => i + 1; inc(inc(5)) }"), 7.into());
    }

    #[test]
    fn let_bindings() {
        assert_eq!(exec("let x = 5"), Value::Unit.into());
    }

    #[test]
    fn block() {
        assert_eq!(exec("{ 5 }"), 5.into());
        assert_eq!(exec("{}"), Value::Unit.into());
        assert_eq!(exec("{ 5; }"), Value::Unit.into());
        assert_eq!(exec("{ let x = 5; }"), Value::Unit.into());
        assert_eq!(exec("{ let x = 5; x }"), 5.into());
    }

    #[test]
    fn nested_block() {
        assert_eq!(exec("{ let t = { let x = 5; let y = 6; x }; t }"), 5.into())
    }

    #[test]
    fn nested_functions() {
        // assert_eq!(exec("(fn x => fn y => x)(1)(2)"), 1.into());
        assert_eq!(exec("(fn x => fn y => x + y)(1)(2)"), 3.into());
        // assert_eq!(
        // exec("(fn () => { let x = 5; let f = fn () => x; f() })()"),
        // 5.into()
        // );
    }

    #[test]
    fn test_closed_upvalues() {
        // the following test accesses x after it is popped off stack, i.e. checks the value is retained
        assert_eq!(exec("(fn () => { let x = 5; fn () => x })()()"), 5.into());
    }

    #[test]
    fn test_closed_params() {
        assert_eq!(exec("(fn x => fn () => x)(4)()"), 4.into());
    }

    #[test]
    fn main_function() {
        assert_eq!(exec_prog("fn main() -> Int { 5 }"), 5.into());
    }

    #[test]
    fn fn_hoisting() {
        assert_eq!(
            exec_prog("fn main() -> Int { f() } fn f() -> Int { 5 }"),
            5.into()
        );
    }

    #[test]
    fn simple_recursion() {
        assert_eq!(exec_prog("fn main() -> Int { fac(5) } fn fac(n: Int) -> Int { if n <= 0 { 1 } else { n * fac(n - 1) } }"), 120.into());
        assert_eq!(exec_prog("fn main() -> Int { fib(10) } fn fib(n: Int) -> Int { if n <= 0 { 1 } else { fib(n - 1) + fib(n - 2) } }"), 144.into());
    }

    #[test]
    fn local_assignment() {
        assert_eq!(exec("{ var x = 5; x = 10; x }"), 10.into());
    }

    #[test]
    fn tuple() {
        //     assert_eq!(
        //         exec("(1,2,3)"),
        //         Value::Tuple(vec![1.into(), 2.into(), 3.into()])
        //     );
    }

    #[test]
    fn set_upvalue() {
        assert_eq!(
            exec_prog("fn main() -> Int { var x = 5; (fn () => x = x + 1)(); x }"),
            6.into()
        );
    }

    #[test]
    fn while_loop() {
        assert_eq!(
            exec_prog("fn main() -> Int { var i = 0; while i < 20 { i = i + 1 }; i }"),
            20.into()
        );
    }
}
