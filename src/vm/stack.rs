use crate::codegen::Closure;

#[derive(Debug)]
pub(crate) struct StackFrame {
    pub closure: Closure,
    pub offset: usize, // frame pointer represented as offset from the bottom of the vm stack
    pub ip: *const u8,
}

impl StackFrame {
    pub fn new(closure: Closure, offset: usize) -> Self {
        // the lamnbda won't move in memory as the closure only holds an rc, also won't get resized and moved as by this point the code generation is done
        let ip: *const u8 = &closure.lambda.chunk.code[0];
        Self {
            closure,
            offset,
            ip,
        }
    }
}
