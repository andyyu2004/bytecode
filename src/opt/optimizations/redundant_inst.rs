use crate::codegen::Opcode;

pub(crate) fn remove_redundant_inst(code: &mut Vec<u8>) {
    for i in (1..code.len()).rev() {
        println!("{}", i);
        // remove all occurences of OP_UNIT followed by an OP_POP
        println!("codes: {:x} {:x}", code[i-1], code[i]);
        if code[i - 1] == Opcode::Unit && code[i] == Opcode::Pop {
            code.drain(i-1..=i);
        }
    }
}
