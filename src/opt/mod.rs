mod optimizations;

use crate::codegen::*;

pub(crate) struct Optimizer<'a> {
    bytecode: &'a mut Lambda,
}

impl<'a> Optimizer<'a> {
    pub fn new(bytecode: &'a mut Lambda) -> Self {
        Self { bytecode }
    }

    pub fn opt(&mut self) {
        optimizations::remove_redundant_inst(&mut self.bytecode.chunk.code);
    }
}
