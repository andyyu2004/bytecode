use super::parse_args;

lazy_static! { pub static ref FLAGS: u128 = get_flags(); }

pub const FDEBUG    : u128 = 0x01; // show debug output
pub const FVERBOSE  : u128 = 0x02;
pub const FBYTECODE : u128 = 0x04; // generate bytecode only, no exec

pub fn get_flags() -> u128 {
    let matches = parse_args();
    let mut flags: u128 = 0;
    if matches.is_present("debug")    { flags |= FDEBUG }
    if matches.is_present("verbose")  { flags |= FVERBOSE }
    if matches.is_present("bytecode") { flags |= FBYTECODE }
    println!("flags: {:x}", flags);
    flags
}
