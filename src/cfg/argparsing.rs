use clap::{Arg, App, ArgMatches};

pub fn parse_args<'a>() -> ArgMatches<'a> {
    App::new("compiler")
        .arg(Arg::with_name("verbose")
             .short("v")
             .long("verbose")
             .takes_value(false))
        .arg(Arg::with_name("file")
             .short("f")
             .takes_value(true)
             .long("file"))
        .arg(Arg::with_name("debug")
             .short("d")
             .long("debug")
             .takes_value(false))
        .arg(Arg::with_name("bytecode")
             .short("b")
             .long("bytecode")
             .takes_value(false))
        .get_matches()
}
