{}
{ 5 }
{ 5; }
{ let x = 5 }
{ let x = 5; }
{ let x = 5; x }
{ let x = 5; x; }
(fn () => { let x = 5; fn () => x })()()
(fn () => { let x = 5; fn () => x })()
(fn () => { let x = 5; fn () => x })()()
fn x => x
(fn x => x)(4)
(fn x => { x })(4)
(fn x => { x; })(4)
fn x => fn y => x
(fn x => fn y => x)(3)(4)
(fn () => { let x = 5; fn () => x })()(
(fn () => { let x = 5; fn () => x })()()
fn () => { let x = 5; fn () => x })()()
(fn () => { let x = 5; fn () => x })()()
(fn x = fn () => x)
(fn x => fn () => x)
(fn x => fn () => x)()
(fn x => fn () => x)(3)
(fn x => fn () => x)()
(fn x => fn () => x)(3)
(fn x => fn () => x)(3)()
(fn x => fn () => x)(3)(3)
(fn (x, y) => fn () => y)(3)()
(fn (x, y) => fn () => y)(3, 4)()
(fn (x, y) => fn () => x)(3, 4)()
(fn (x, y) => fn () => y)(3, 4)()
(fn (x, y) => fn (x) => y)(3, 4)(3)
(fn (x, y) => fn (x) => x)(3, 4)(5)
(fn (x, y) => fn (x) => y)(3, 4)(3)
(fn (x, y) => fn (x) => { let t = 4; y})(3, 4)(3)
(fn (x, y) => fn (x) => { let t = 4; t})(3, 4)(3)
(fn (x, y) => fn (x) => { let t = 4; let a = 3; let b = 4;  x})(3, 4)(3)
(fn (x, y) => fn (x) => { let t = 4; let a = 3; let b = 4;  y})(3, 4)(3)
(fn (x, y) => fn (x) => { let t = 4; let a = 3; let b = 4;  x})(3, 4)(3)
(fn (x, y) => fn (x) => { let t = 4; let a = 32; let b = 44; let c = 2323;  x})(3, 4)(3)
(fn (x, y) => fn (x) => { let t = 4; let a = 32; let b = 44; let c = 2323;  y})(3, 4)(3)
(fn (x, y) => fn (x) => { let t = 4; let a = 32; let b = 44; let e = y; let c = 2323;  e})(3, 4)(3)
(fn (x, y) => fn (x) => { let t = 4; t})(3, 4)(3)
(fn (x, y) => fn (x) => { let t = 4; let a = 32; let b = 44; let e = y; let c = 2323;  e})(3, 4)(3)
(fn (x, y) => fn (x) => { let t = 4; let a = 32; let b = 44; let e = y; let c = 2323;  y})(3, 4)(3)
(fn (x, y) => fn (x) => { let t = 4; let a = 32; let b = 44; let e = y; let c = 2323; e})(3, 4)(3)
fn x => { let t = x; x }
(fn x => { let t = x; x })(3)
(fn x => { let t = x; t })(3)
(fn x => { let t = x; x })(3)
(fn x => { let t = x; t })(3)
(fn x => { let t = x; let y = t; y })(3)
(fn x: Bool => { let t = x; let y = t; y })(3)
(fn x: Int => { let t = x; let y = t; y })(3)
(fn x => { let t = x; let y: Int = t; y })(3)
(fn x => { let t = x; let y: Bool = t; y })(3)
(fn x => { let t: Bool = x;  y })(3)
(fn x => { let t: Bool = x;  t })(3)
(fn x => { let t = x; let y: Bool = t; y })(3)
(fn x => { let t = x; let y = t; y })(3)
(fn x => { let t = x; let y = t; t })(3)
(fn x => fn () => x)(3)
(fn x => fn () => x)(3)()
{ let x = 5; let y = 6; x }
let t = { let x = 5; let y = 6; x }
{ let t = { let x = 5; let y = 6; x }; t }
{ let x = 5; let y = 6; x }
{ let t = { let x = 5; let y = 6; x }; t }
{ let t = { let x = 5; let y = 6; x };  }
{ { let x = 5; let y = 6; x }  }
{ let t = { let x = 5; let y = 6; x };  }
{ let t = { let x = 5; let y = 6; x }; t  }
3
23
13
324
234
fn x => x
{ let inc = fn i => i + 1; inc(inc(5)) }
()
ls
()
(1,2,3)
(1,2,3, 5, 6, 7,)
{ var x = 5; x = 10; x }
{ var x = 5; { x = 10 }; x }
2 + 5
2 + 5 + 10
2 + 5
exit
:q
2+2
2+5
 (fn () => { let x = 5; fn () => x })()() ;
 (fn () => { let x = 5; fn () => x })()()
(fn x => fn y => x + y)(1)(2)
2
5
exit
